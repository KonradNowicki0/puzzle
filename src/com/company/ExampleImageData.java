package com.company;

import java.util.ArrayList;
import java.util.List;

public class ExampleImageData implements ImageDao {


    private List<Imagleable> imageDaoList = new ArrayList<>();


    @Override
    public void add(Imagleable imagleable) {
        imageDaoList.add(imagleable);
    }




    @Override
    public void generateExampleData() {
        imageDaoList.add(new Cat("kot1.jpg"));
        imageDaoList.add(new Cat("kot2.jpg"));
        imageDaoList.add(new Cat("kot3.jpg"));
        imageDaoList.add(new Dog("pies1.jpg"));
        imageDaoList.add(new Dog("pies2.jpg"));
        imageDaoList.add(new Dog("pies3.jpg"));

    }

    @Override
    public List<Imagleable> findAll() {
        return imageDaoList;
    }



}
