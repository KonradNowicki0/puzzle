package com.company;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import static com.sun.glass.ui.Cursor.setVisible;

public class Main extends JFrame implements ActionListener {



    private ImageDao imageDao;

    private List<Imagleable> imagleables = new ArrayList<>();

    public List<Imagleable> getImagleables() {
        return imagleables;
    }

    public void setImagleables(List<Imagleable> imagleables) {
        this.imagleables = imagleables;
    }

    public ImageDao getImageDao() {
        return imageDao;
    }

    public void setImageDao(ImageDao imageDao) {
        this.imageDao = imageDao;
        imageDao.generateExampleData();
        setImagleables(imageDao.findAll());
    }

    private JPanel jPanel;



    private ImageGenerator simpleImageGenerator;

    public Main(){
        createFrameLayout();
        setImageDao(new ExampleImageData());
//        Random rn = new Random();
//        HashSet<String> animalsSet = new HashSet<>();
//        while(animalsSet.size() < 4){
//            animalsSet.add(animals.get(rn.nextInt(6)).getFileName());
//        }
//        String[] newAnimals = animalsSet.toArray(new String[4]);


        simpleImageGenerator = new SimpleImageGenerator(getImageDao().findAll());

        try {
            imagleables = simpleImageGenerator.generate();
        } catch (NoImageException e) {
            e.printStackTrace();
        }
        for (int i = 1; i <= 4; i++) {
            JButton jButton = new JButton("");
            ImageIcon imageIcon = new
                    ImageIcon(imagleables.get(i-1).getFileName());
            jButton.setIcon(imageIcon);
            jButton.addActionListener(this);
            jButton.setName(imagleables.get(i-1).getClass().getSimpleName());
            jPanel.add(jButton);
        }
        add(jPanel);
    }

    private void createFrameLayout() {
        setSize(500, 500); //ustawia wielkość okna
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //sprawia, ze bedzie dzialal exit
        setVisible(true); //okno bedzie widoczne

        setLayout(new BoxLayout(getContentPane(),BoxLayout.Y_AXIS));
        JLabel jLabel = new JLabel("Co nie jest kotem");
        jLabel.setFont(new Font("Serif", Font.PLAIN,24));
        jLabel.setForeground(Color.MAGENTA);

        add(jLabel);

        jPanel = new JPanel();
        jPanel.setLayout(new GridLayout(2,2));
    }

    public static void main(String[] args) {

        //bedzie tworzone w tle
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Main(); //tworzenie okna
            }
        });


    }


    @Override
    public void actionPerformed(ActionEvent e) {
        JButton clickedbutton = (JButton) e.getSource();
        System.out.println(clickedbutton.getName());
        int theNumberWithSameClass = 0;
        for (Imagleable imagleable: getImagleables()){
            if(imagleable.getClass().getSimpleName().equals(clickedbutton.getName())){
                theNumberWithSameClass++;
            }
        }
        if (theNumberWithSameClass == 1){
            JOptionPane.showMessageDialog(this, "Gratulacje poprawna odpowiedź");
        } else{
            JOptionPane.showMessageDialog(this, "Niepoprawna odpowiedź");
        }

    }
}
